CREATE TABLE comment (
  id             INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
  competition_id INT                            NOT NULL,
  date           DATE,
  text           VARCHAR(1000),
  INDEX competition_id_inx (competition_id),
  FOREIGN KEY (competition_id)
  REFERENCES competition (id)
);