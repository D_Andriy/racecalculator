package com.fau.util;

import com.fau.competition.domain.DriverResultItem;
import com.fau.driver.domain.Driver;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DriversResultItemBuilder {

    private static final LocalTime PENALTY_NOT_PRESENT = LocalTime.of(00, 00);

    public static List<DriverResultItem> createDriversResultItem(List<Driver> drivers, LocalTime chipBoardTime,
                                                                 LocalTime chipFrontTime, LocalTime falseStartTime) {
        return drivers.stream()
                .map(driver -> {
                    DriverResultItem item = new DriverResultItem();
                    item.setDriverCarCategory(driver.getCarCategory());
                    item.setDriverNumber(driver.getNumber());
                    item.setDriverName(driver.getSurname() + " " + driver.getName());
                    item.setDriverCarMark(driver.getCarMark());
                    item.setFirstLap(driver.getLaps().get(0).getTime());
                    item.setSecondLap(driver.getLaps().get(1).getTime());
                    item.setThirdLap(driver.getLaps().get(2).getTime());

                    //set penalty time if penalty is present
                    item.setLap1ChipBoard(driver.getLaps().get(0).isChipBoard() ? chipBoardTime : PENALTY_NOT_PRESENT);
                    item.setLap1ChipFront(driver.getLaps().get(0).isChipFront() ? chipFrontTime : PENALTY_NOT_PRESENT);
                    item.setLap1FalseStart(driver.getLaps().get(0).isFalseStart() ? falseStartTime : PENALTY_NOT_PRESENT);
                    item.setLap2ChipBoard(driver.getLaps().get(1).isChipBoard() ? chipBoardTime : PENALTY_NOT_PRESENT);
                    item.setLap2ChipFront(driver.getLaps().get(1).isChipFront() ? chipFrontTime : PENALTY_NOT_PRESENT);
                    item.setLap2FalseStart(driver.getLaps().get(1).isFalseStart() ? falseStartTime : PENALTY_NOT_PRESENT);
                    item.setLap3ChipBoard(driver.getLaps().get(2).isChipBoard() ? chipBoardTime : PENALTY_NOT_PRESENT);
                    item.setLap3ChipFront(driver.getLaps().get(2).isChipFront() ? chipFrontTime : PENALTY_NOT_PRESENT);
                    item.setLap3FalseStart(driver.getLaps().get(2).isFalseStart() ? falseStartTime : PENALTY_NOT_PRESENT);

                    //create parameter for ResultUtil()
                    List<TimeSummator> timeArray = Arrays.asList(new TimeSummator(item.getFirstLap().toNanoOfDay(),
                                    item.getLap1ChipBoard().toNanoOfDay() +
                                            item.getLap1ChipFront().toNanoOfDay() +
                                            item.getLap1FalseStart().toNanoOfDay()),
                            new TimeSummator(item.getSecondLap().toNanoOfDay(), item.getLap2ChipBoard().toNanoOfDay() +
                                    item.getLap2ChipFront().toNanoOfDay() +
                                    item.getLap2FalseStart().toNanoOfDay()),
                            new TimeSummator(item.getThirdLap().toNanoOfDay(), item.getLap3ChipBoard().toNanoOfDay() +
                                    item.getLap3ChipFront().toNanoOfDay() +
                                    item.getLap3FalseStart().toNanoOfDay()));

                    //calculate average by total time for driver
                    item.setAverage(LocalTime.ofNanoOfDay(ResultUtil.calculateResult(timeArray)));

                    return item;
                }).sorted(Comparator.comparing(DriverResultItem::getAverage))
                .collect(Collectors.toList());
    }
}
