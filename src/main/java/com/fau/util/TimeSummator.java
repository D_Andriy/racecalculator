package com.fau.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TimeSummator {

    private long time;
    private long penalty;
}
