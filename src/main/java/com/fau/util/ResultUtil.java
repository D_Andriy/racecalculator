package com.fau.util;

import java.util.List;

public class ResultUtil {

    public static long calculateResult(List<TimeSummator> timeArray) {

        return timeArray.stream()
                .mapToLong(l -> l.getTime() + l.getPenalty())
                .sorted()
                .limit(2)
                .reduce(0L, (l1, l2) -> (l1 + l2) / 2);
    }
}
