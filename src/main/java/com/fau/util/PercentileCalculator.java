package com.fau.util;

import java.time.LocalTime;
import java.util.List;

public class PercentileCalculator {

    public static LocalTime percentile(List<LocalTime> time, double percentileNumber) {
        time.stream().sorted();
        int index = (int)Math.ceil((percentileNumber / 100) * time.size());
        return time.get(index - 1);
    }
}
