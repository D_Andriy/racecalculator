package com.fau;

import com.fau.competition.service.CompetitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {

    private final CompetitionService competitionService;

    @Autowired
    public HomeController(CompetitionService competitionService) {
        this.competitionService = competitionService;
    }

    @GetMapping
    public String startPage(Model model) {
        model.addAttribute("competitions", competitionService.getActiveCompetitions());
        return "index";
    }
}
