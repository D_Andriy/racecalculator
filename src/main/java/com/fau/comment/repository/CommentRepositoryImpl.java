package com.fau.comment.repository;

import com.fau.comment.domain.Comment;
import com.fau.comment.repository.rowmapper.CommentRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CommentRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void addComment(String text, int competitionId) {
        Object[] params = new Object[]{competitionId, text};
        String sql = "INSERT  INTO comment (competition_id, date, text) VALUES( ?, DATE(NOW()), ?)";
        jdbcTemplate.update(sql, params);
    }

    @Override
    public List<Comment> allComments(int competitionId) {
        Object[] param = new Object[]{competitionId};
        String sql = "SELECT\n" +
                     "c.id,\n" +
                     "c.date,\n" +
                     "c.text\n" +
                     "FROM comment c\n" +
                     "WHERE c.competition_id = ?";
        return jdbcTemplate.query(sql, param, new CommentRowMapper());
    }

    @Override
    public void deleteComment(int id) {
        Object[] param = new Object[]{id};
        String sql = "DELETE FROM comment WHERE id = ?";
        jdbcTemplate.update(sql, param);
    }
}
