package com.fau.comment.repository;

import com.fau.comment.domain.Comment;

import java.util.List;

public interface CommentRepository {

    void addComment(String text, int competitionId);

    List<Comment> allComments(int competitionId);

    void deleteComment(int id);
}
