package com.fau.comment.repository.rowmapper;

import com.fau.comment.domain.Comment;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class CommentRowMapper implements RowMapper<Comment> {

   @Override
    public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
       Comment comment = new Comment();
       comment.setId(rs.getInt("id"));
       comment.setCommentDate(LocalDate.parse(rs.getString("date")));
       comment.setText(rs.getString("text"));
        return comment;
    }
}
