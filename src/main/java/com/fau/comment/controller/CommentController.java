package com.fau.comment.controller;

import com.fau.comment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/comment")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/{competitionId}/show")
    public String showComments(@PathVariable int competitionId, Model model) {
        model.addAttribute("competitionId", competitionId);
        model.addAttribute("comments", commentService.allComments(competitionId));
        return "comments";
    }

    @PostMapping("/{competitionId}/add")
    public String addComment(@PathVariable int competitionId, @RequestParam String text) {
        commentService.addComment(text, competitionId);

        return "redirect:/comment/" + String.valueOf(competitionId) + "/show";
    }

    @PostMapping("/{id}/delete")
    public String deleteComment(@PathVariable int id) {
        commentService.deleteComment(id);
        return "redirect:/competition/archive";
    }
}
