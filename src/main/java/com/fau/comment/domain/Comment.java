package com.fau.comment.domain;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Comment {

    private int id;
    private LocalDate commentDate;
    private String text;
}
