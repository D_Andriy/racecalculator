package com.fau.comment.service;

import com.fau.comment.domain.Comment;

import java.util.List;

public interface CommentService {

    void addComment(String text, int competitionId);

    List<Comment> allComments(int competitionId);

    void deleteComment(int id);
}
