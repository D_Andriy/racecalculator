package com.fau.comment.service;

import com.fau.comment.domain.Comment;
import com.fau.comment.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public void addComment(String text, int competitionId) {
        commentRepository.addComment(text, competitionId);
    }

    @Override
    public List<Comment> allComments(int competitionId) {
        return commentRepository.allComments(competitionId);
    }

    @Override
    public void deleteComment(int id) {
        commentRepository.deleteComment(id);
    }
}
