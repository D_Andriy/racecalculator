package com.fau.security.service;

import com.fau.security.model.RoleType;
import com.fau.security.model.User;
import com.fau.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ShaPasswordEncoder shaPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, ShaPasswordEncoder shaPasswordEncoder) {
        this.userRepository = userRepository;
        this.shaPasswordEncoder = shaPasswordEncoder;

    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void addUser(User user) {
        user.setPassword(shaPasswordEncoder.encodePassword(user.getPassword(), null));
        user.setRole(RoleType.USER.name());
        userRepository.addUser(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers(RoleType.USER.name());
    }

    @Override
    public void deleteUser(String username) {
        userRepository.deleteUser(username);
    }
}
