package com.fau.security.service;

import com.fau.security.model.User;

import java.util.List;

public interface UserService {

    User findByUsername(String username);

    void addUser(User user);

    List<User> getAllUsers();

    void deleteUser(String username);
}
