package com.fau.security.repository;

import com.fau.security.model.User;

import java.util.List;

public interface UserRepository {

    User findByUsername(String username);

    void addUser(User user);

    List<User> getAllUsers(String role);

    void deleteUser(String username);
}
