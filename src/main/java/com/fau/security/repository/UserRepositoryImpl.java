package com.fau.security.repository;

import com.fau.security.model.User;
import com.fau.security.repository.rowmapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public User findByUsername(String username) {
        Object[] param = new Object[]{username};
        String findByUsername = "SELECT " +
                                "u.username," +
                                "u.password," +
                                "u.role\n " +
                                "FROM user u WHERE username = ?";
        return jdbcTemplate.queryForObject(findByUsername, param, new UserRowMapper());
    }

    @Override
    public void addUser(User user) {
        Object[] params = new Object[]{user.getUsername(), user.getPassword(), user.getRole()};
        String addUser = "INSERT INTO user (username, password, role) VALUES( ?, ?, ? )";
        jdbcTemplate.update(addUser, params);
    }

    @Override
    public List<User> getAllUsers(String role) {
        Object[] param = new Object[]{role};
        String getAllUsers = "SELECT " +
                             "u.username," +
                             "u.password," +
                             "u.role\n " +
                             "FROM user u WHERE role = ?";
        return jdbcTemplate.query(getAllUsers, param, new UserRowMapper());
    }

    @Override
    public void deleteUser(String username) {
        Object[] param = new Object[]{username};
        String deleteUser = "DELETE FROM user " +
                            "WHERE username = ?";
        jdbcTemplate.update(deleteUser, param);
    }
}
