package com.fau.security.controller;

import com.fau.security.model.User;
import com.fau.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/add")
    public String addUser(User user) {
        userService.addUser(user);
        return "redirect:/login";
    }

    @GetMapping("/all")
    public String getAllUsers(Model model){
        model.addAttribute("users", userService.getAllUsers());
        return "usersList";
    }

    @PostMapping("/{username}/delete")
    public String deleteUser(@PathVariable String username){
        userService.deleteUser(username);
        return "redirect:/user/all";
    }
}
