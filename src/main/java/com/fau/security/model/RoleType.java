package com.fau.security.model;

public enum RoleType {

    USER,
    ADMIN;
}
