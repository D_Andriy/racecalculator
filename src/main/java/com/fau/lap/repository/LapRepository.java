package com.fau.lap.repository;

import com.fau.lap.domain.Lap;

public interface LapRepository {

    void saveQualificationLap(Lap lap, int driverID);
}
