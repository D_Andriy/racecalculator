package com.fau.lap.service;

import com.fau.lap.domain.Lap;

public interface LapService {

    void saveQualificationLap(Lap lap, int driverId);
}
