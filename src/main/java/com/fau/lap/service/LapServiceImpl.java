package com.fau.lap.service;

import com.fau.lap.domain.Lap;
import com.fau.lap.repository.LapRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LapServiceImpl implements LapService {

    private final LapRepository lapRepository;

    @Autowired
    public LapServiceImpl(LapRepository lapRepository) {
        this.lapRepository = lapRepository;
    }

    @Override
    public void saveQualificationLap(Lap lap, int driverId) {
        lapRepository.saveQualificationLap(lap, driverId);
    }
}
