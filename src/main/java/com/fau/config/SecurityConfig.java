package com.fau.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final ShaPasswordEncoder getShaPasswordEncoder;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService, ShaPasswordEncoder getShaPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.getShaPasswordEncoder = getShaPasswordEncoder;
    }

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
            auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(getShaPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
              http
                   .authorizeRequests()
                   .expressionHandler(webExpressionHandler())
                   .antMatchers("/competition/new/**",
                           "/competition/{competitionId}/qualification/**",
                           "/competition/general**",
                           "/competition/{competitionId}/close",  "/user/all").hasAuthority("ADMIN")
                      .antMatchers("/comment/{competitionId}/add").hasAuthority("USER")
                   .anyRequest().permitAll()
                .and()
                   .formLogin()
                   .loginPage("/login")
                   .usernameParameter("username")
                   .passwordParameter("password")
                .and()
                   .rememberMe()
                .and()
                   .logout()
                   .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                   .logoutSuccessUrl("/")
                .and()
                    .csrf()
                    .disable();
    }

    private SecurityExpressionHandler<FilterInvocation> webExpressionHandler() {
        DefaultWebSecurityExpressionHandler defaultWebSecurityExpressionHandler = new DefaultWebSecurityExpressionHandler();
        defaultWebSecurityExpressionHandler.setRoleHierarchy(roleHierarchy());
        return defaultWebSecurityExpressionHandler;
    }

    @Bean
    public RoleHierarchyImpl roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy("ADMIN > USER");
        return roleHierarchy;
    }
}