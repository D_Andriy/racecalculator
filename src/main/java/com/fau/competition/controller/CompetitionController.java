package com.fau.competition.controller;

import com.fau.competition.domain.Competition;
import com.fau.competition.domain.DriverResultItem;
import com.fau.competition.service.CompetitionService;
import com.fau.driver.domain.CarCategoryType;
import com.fau.driver.domain.Driver;
import com.fau.driver.service.DriverService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/competition")
public class CompetitionController {

    private final CompetitionService competitionService;
    private final DriverService driverService;

    public CompetitionController(CompetitionService competitionService, DriverService driverService) {
        this.competitionService = competitionService;
        this.driverService = driverService;
    }

    @GetMapping("/new")
    public String competitionFormation() {
        return "competitionFormation";
    }

    @PostMapping("/create")
    public String createCompetition(Competition competition) {
        competition.setActive(true);
        int competitionId = competitionService.createCompetition(competition);
        return "redirect:/competition/" + competitionId + "/qualification";
    }

    @ResponseBody
    @GetMapping("/{competitionId}/drivers")
    public List<Driver> drivers(@PathVariable int competitionId) {
        return driverService.getDriverList(competitionId);
    }

    @GetMapping("/{competitionId}/qualification/result")
    public String qualificationResult(Model model, @PathVariable int competitionId) {
        model.addAttribute("drivers", competitionService.getQualificationResult(competitionId));
        model.addAttribute("competitionId", competitionId);
        return "qualificationResult";
    }

    @GetMapping("/{competitionId}/qualification")
    public String qualification(@PathVariable int competitionId, Model model) {
        model.addAttribute("competitionId", competitionId);
        return "qualification";
    }

    @GetMapping("/{competitionId}/general")
    public String generalCompetition(@PathVariable int competitionId, Model model) {
        model.addAttribute("drivers", driverService.getOrderedDriversByQualificationResult(competitionId));
        model.addAttribute("competitionId", competitionId);
        return "generalCompetition";
    }

    @GetMapping("/{competitionId}/general/result")
    public String getGeneralResult(@PathVariable int competitionId, Model model) {
        List<DriverResultItem> driverResultItems = competitionService.calculateResult(competitionId);
        model.addAttribute("standard",
                             driverResultItems
                                     .stream()
                                     .filter(d -> d.getDriverCarCategory().equals(CarCategoryType.STANDARD.getCategoryName()))
                                     .collect(Collectors.toList()));

        model.addAttribute("front",
                             driverResultItems
                                     .stream()
                                     .filter(d -> d.getDriverCarCategory().equals(CarCategoryType.SPORT_FRONT.getCategoryName()))
                                     .collect(Collectors.toList()));

        model.addAttribute("rear",
                             driverResultItems
                                     .stream()
                                     .filter(d -> d.getDriverCarCategory().equals(CarCategoryType.SPORT_REAR.getCategoryName()))
                                     .collect(Collectors.toList()));

        model.addAttribute("activeCompetitions", competitionService.getActiveCompetitions());
        model.addAttribute("absolute", driverResultItems);
        model.addAttribute("competitionId", competitionId);

        return "generalResult";
    }

    @GetMapping("/search")
    public String searchByCity(@RequestParam("city") String city, Model model) {
        model.addAttribute("competitions", competitionService.searchByCity(city));
        return "citySearch";
    }

    @GetMapping("/by/driver/search")
    public String searchByDriverSurname(@RequestParam String driverSurname, Model model) {
        model.addAttribute("competitions", competitionService.getCompetitionsByDriverSurname(driverSurname));
        return "archive";
    }

    @GetMapping("date/search")
    public String searchByDatesRange(@RequestParam String dateFrom,
                                     @RequestParam String dateBefore, Model model) {
        model.addAttribute("competitions", competitionService.searchByDateRange(
                              LocalDate.parse(dateFrom), LocalDate.parse(dateBefore)));
        return "archive";
    }

    @GetMapping("/archive")
    public String allCompetition(Model model) {
        model.addAttribute("competitions", competitionService.getAllCompetitions());
        return "archive";
    }

    @GetMapping("/top/fast")
    public String getTopFastCompetitionSeason(Model model) {
        model.addAttribute("competitions", competitionService.getTopFastCompetitionSeason());
        return "topFastCompetition";
    }

    @PostMapping("/{competitionId}/close")
    public String closeCompetition(@PathVariable int competitionId) {
        competitionService.closeCompetition(competitionId);
        return "redirect:/";
    }
}
