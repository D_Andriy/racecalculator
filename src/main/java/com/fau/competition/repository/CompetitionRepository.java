package com.fau.competition.repository;

import com.fau.competition.domain.Competition;
import com.fau.competition.domain.FastCompetitionSeason;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface CompetitionRepository {

    int createCompetition(Competition competition);

    List<Competition> getActiveCompetitions();

    List<Competition> getAllCompetitions();

    Competition getCompetitionPenalties(int competitionId);

    List<Competition> getCompetitionsByDriverSurname(String driverSurname);

    List<Competition> searchByCity(String city);

    List<Competition> searchByDateRange(LocalDate dateFrom, LocalDate dateBefore);

    List<FastCompetitionSeason> getTopFastCompetitionSeason();

    void closeCompetition(int competitionId);
}
