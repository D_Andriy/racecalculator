package com.fau.competition.repository;

import com.fau.competition.domain.Competition;
import com.fau.competition.domain.FastCompetitionSeason;
import com.fau.competition.repository.rowmapper.CompetitionPenaltyRowMapper;
import com.fau.competition.repository.rowmapper.CompetitionRowMapper;
import com.fau.competition.repository.rowmapper.FastCompetitionSeasonRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CompetitionRepositoryImpl implements CompetitionRepository {

    private static final int TOP_COUNT_RATING = 10;
    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert insertCompetition;

    @Autowired
    public CompetitionRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.insertCompetition = new SimpleJdbcInsert(dataSource)
                .withTableName("competition")
                .usingGeneratedKeyColumns("id");
    }

    @Override
    public int createCompetition(Competition competition) {
        Map<String, Object> parameters = new HashMap<>(8);
        parameters.put("name", competition.getCompetitionName());
        parameters.put("date", Date.valueOf(competition.getDate()));
        parameters.put("city", competition.getCompetitionCity());
        parameters.put("is_active", competition.isActive());
        parameters.put("chip_board", competition.getChipBoard().toNanoOfDay());
        parameters.put("chip_front", competition.getChipFront().toNanoOfDay());
        parameters.put("false_start", competition.getFalseStart().toNanoOfDay());
        Number newId = insertCompetition.executeAndReturnKey(parameters);
        competition.setId(newId.intValue());

        return newId.intValue();
    }

    @Override
    public List<Competition> getActiveCompetitions() {
        String listCompetitions = "SELECT\n" +
                                  "  c.id,\n" +
                                  "  c.name,\n" +
                                  "  c.date,\n" +
                                  "  c.city\n" +
                                  "FROM competition c\n" +
                                  "WHERE is_active = TRUE";
        return jdbcTemplate.query(listCompetitions, new CompetitionRowMapper());
    }

    @Override
    public List<Competition> getAllCompetitions() {
        String allCompetitions = "SELECT\n" +
                                 "  id,\n" +
                                 "  name,\n" +
                                 "  date,\n" +
                                 "  city\n" +
                                 "FROM competition c\n" +
                                 "ORDER BY c.date";
        return jdbcTemplate.query(allCompetitions, new CompetitionRowMapper());
    }

    @Override
    public List<Competition> getCompetitionsByDriverSurname(String driverSurname) {
        Object[] param = new Object[]{driverSurname};
        String competitionsWithDriver = "SELECT DISTINCT\n" +
                                        "  c.id,\n" +
                                        "  c.name,\n" +
                                        "  c.date,\n" +
                                        "  c.city\n" +
                                        "FROM competition c\n" +
                                        "  JOIN competition_driver cd ON c.id = cd.competition_id\n" +
                                        "  JOIN driver d ON cd.driver_id = d.id\n" +
                                        "                   AND d.surname = ?\n" +
                                        "ORDER BY c.date";
        return jdbcTemplate.query(competitionsWithDriver, param, new CompetitionRowMapper());
    }

    @Override
    public Competition getCompetitionPenalties(int competitionId) {
        Object[] param = new Object[]{competitionId};
        String competitionPenalties = "SELECT " +
                                      "chip_board, chip_front, false_start " +
                                      "FROM competition " +
                                      "WHERE id = ?";
        return jdbcTemplate.queryForObject(competitionPenalties, param, new CompetitionPenaltyRowMapper());
    }

    @Override
    public List<Competition> searchByCity(String city) {
        Object[] param = new Object[]{city};
        String citySearch = "SELECT " +
                            "c.id, c.name, " +
                            "c.date, c.city " +
                            "FROM competition c " +
                            "WHERE city = ?\n" +
                            "ORDER BY c.date";
        return jdbcTemplate.query(citySearch, param, new CompetitionRowMapper());
    }

    @Override
    public List<Competition> searchByDateRange(LocalDate dateFrom, LocalDate dateBefore) {
        Object[] params = new Object[]{Date.valueOf(dateFrom), Date.valueOf(dateBefore)};
        String getByDatesRange = "SELECT\n" +
                                 "  c.id,\n" +
                                 "  c.name,\n" +
                                 "  c.date,\n" +
                                 "  c.city\n" +
                                 "FROM competition c\n" +
                                 "WHERE date > ? AND date < ?\n" +
                                 "ORDER BY date";
        return jdbcTemplate.query(getByDatesRange, params, new CompetitionRowMapper());
    }

    @Override
    public List<FastCompetitionSeason> getTopFastCompetitionSeason() {
        String fastCompetitions = "SELECT\n" +
                                  "  c.id,\n" +
                                  "  c.name,\n" +
                                  "  c.date,\n" +
                                  "  c.city,\n" +
                                  "  SUM(l.time) AS sum_laps,\n" +
                                  "  COUNT(DISTINCT cd.driver_id) AS driver_count\n" +
                                  "FROM competition c\n" +
                                  "  JOIN competition_driver cd ON c.id = cd.competition_id\n" +
                                  "  JOIN driver d ON d.id = cd.driver_id\n" +
                                  "  JOIN lap l ON d.id = l.driver_id\n" +
                                  "WHERE YEAR(c.date) = YEAR(CURDATE())\n" +
                                  "GROUP BY c.id\n" +
                                  "ORDER BY driver_count DESC, sum_laps\n" +
                                  "LIMIT " + TOP_COUNT_RATING;

        return jdbcTemplate.query(fastCompetitions, new FastCompetitionSeasonRowMapper());
    }

    @Override
    public void closeCompetition(int competitionId) {
        Object[] param = new Object[]{competitionId};
        jdbcTemplate.update("UPDATE Competition\n" +
                                 "SET is_active = FALSE\n" +
                                 "WHERE id = ?",
                                  param);
    }
}
