package com.fau.competition.repository.rowmapper;

import com.fau.competition.domain.FastCompetitionSeason;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;

public class FastCompetitionSeasonRowMapper implements RowMapper<FastCompetitionSeason> {

    @Override
    public FastCompetitionSeason mapRow(ResultSet rs, int rowNum) throws SQLException {
        FastCompetitionSeason fastCompetition = new FastCompetitionSeason();
        fastCompetition.setCompetitionId(rs.getInt("id"));
        fastCompetition.setCompetitionDate(LocalDate.parse(rs.getString("date")));
        fastCompetition.setCompetitionName(rs.getString("name"));
        fastCompetition.setCompetitionCity(rs.getString("city"));
        fastCompetition.setSumLapsTime(LocalTime.ofNanoOfDay(rs.getLong("sum_laps")));
        fastCompetition.setQuantityDrivers(rs.getInt("driver_count"));

        return fastCompetition;
    }
}
