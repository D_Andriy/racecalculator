package com.fau.competition.repository.rowmapper;

import com.fau.competition.domain.Competition;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;

public class CompetitionPenaltyRowMapper implements RowMapper<Competition> {

    @Override
    public Competition mapRow(ResultSet rs, int rowNum) throws SQLException {
        Competition competition = new Competition();
        competition.setChipBoard(LocalTime.ofNanoOfDay(rs.getLong("chip_board")));
        competition.setChipFront(LocalTime.ofNanoOfDay(rs.getLong("chip_front")));
        competition.setFalseStart(LocalTime.ofNanoOfDay(rs.getLong("false_start")));

        return competition;
    }
}
