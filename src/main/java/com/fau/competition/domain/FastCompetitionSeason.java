package com.fau.competition.domain;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class FastCompetitionSeason {

    private int competitionId;
    private LocalDate competitionDate;
    private String competitionName;
    private String competitionCity;
    private LocalTime sumLapsTime;
    private int quantityDrivers;
}
