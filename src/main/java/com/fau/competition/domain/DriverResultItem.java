package com.fau.competition.domain;

import lombok.Data;

import java.time.LocalTime;

@Data
public class DriverResultItem {

    private int driverNumber;
    private String driverName;
    private String driverCarCategory;
    private String driverCarMark;

    private LocalTime firstLap;
    private LocalTime secondLap;
    private LocalTime thirdLap;

    private LocalTime lap1ChipBoard;
    private LocalTime lap1ChipFront;
    private LocalTime lap1FalseStart;

    private LocalTime lap2ChipBoard;
    private LocalTime lap2ChipFront;
    private LocalTime lap2FalseStart;

    private LocalTime lap3ChipBoard;
    private LocalTime lap3ChipFront;
    private LocalTime lap3FalseStart;

    private LocalTime average;
}
