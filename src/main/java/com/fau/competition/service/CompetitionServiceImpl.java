package com.fau.competition.service;

import com.fau.competition.domain.Competition;
import com.fau.competition.domain.DriverResultItem;
import com.fau.competition.domain.FastCompetitionSeason;
import com.fau.competition.repository.CompetitionRepository;
import com.fau.driver.domain.Driver;
import com.fau.driver.service.DriverService;
import com.fau.lap.domain.Lap;
import com.fau.lap.service.LapService;
import com.fau.util.DriversResultItemBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompetitionServiceImpl implements CompetitionService {

    private final CompetitionRepository competitionRepository;
    private final DriverService driverService;

    @Autowired
    public CompetitionServiceImpl(CompetitionRepository competitionRepository, DriverService driverService) {
        this.competitionRepository = competitionRepository;
        this.driverService = driverService;
    }

    @Override
    public int createCompetition(Competition competition) {
        return competitionRepository.createCompetition(competition);
    }

    @Override
    public List<Competition> getAllCompetitions() {
        return competitionRepository.getAllCompetitions();
    }

    @Override
    public List<Driver> getQualificationResult(int competitionId) {
        return driverService.getOrderedDriversByQualificationResult(competitionId);
    }

    @Override
    public void closeCompetition(int competitionId) {
        competitionRepository.closeCompetition(competitionId);
    }

    @Override
    public List<Competition> searchByCity(String city) {
        return competitionRepository.searchByCity(city);
    }

    @Override
    public List<Competition> getActiveCompetitions() {
        return competitionRepository.getActiveCompetitions();
    }

    @Override
    public List<Driver> getGeneralDriversAndLaps(int competitionId) {

        return driverService.getGeneralDriversAndLaps(competitionId);

    }

    @Override
    public List<Competition> getCompetitionsByDriverSurname(String driverSurname) {
        return competitionRepository.getCompetitionsByDriverSurname(driverSurname);
    }

    @Override
    public List<Competition> searchByDateRange(LocalDate dateFrom, LocalDate dateBefore) {
        return competitionRepository.searchByDateRange(dateFrom, dateBefore);
    }

    @Override
    public List<FastCompetitionSeason> getTopFastCompetitionSeason() {
        return competitionRepository.getTopFastCompetitionSeason();
    }

    /**
     * @return list drivers with data, time, penalty and result time for final view
     */
    @Override
    public List<DriverResultItem> calculateResult(int competitionId) {
        List<Driver> drivers = getGeneralDriversAndLaps(competitionId);
        Competition competitionPenalties = competitionRepository.getCompetitionPenalties(competitionId);

        return DriversResultItemBuilder.createDriversResultItem(
                drivers,
                competitionPenalties.getChipBoard(),
                competitionPenalties.getChipFront(),
                competitionPenalties.getFalseStart()
        );
    }
}
