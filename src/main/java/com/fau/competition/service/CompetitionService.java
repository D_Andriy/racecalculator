package com.fau.competition.service;

import com.fau.competition.domain.Competition;
import com.fau.competition.domain.DriverResultItem;
import com.fau.competition.domain.FastCompetitionSeason;
import com.fau.driver.domain.Driver;

import java.time.LocalDate;
import java.util.List;

public interface CompetitionService {

    int createCompetition(Competition competition);

    List<Competition> getAllCompetitions();

    List<Driver> getQualificationResult(int competitionId);

    void closeCompetition(int competitionId);

    List<Competition> searchByCity(String city);

    List<Competition> getActiveCompetitions();

    List<Driver> getGeneralDriversAndLaps(int competitionId);

    List<Competition> getCompetitionsByDriverSurname(String driverSurname);

    List<Competition> searchByDateRange(LocalDate dateFrom, LocalDate dateBefore);

    List<FastCompetitionSeason> getTopFastCompetitionSeason();

    List<DriverResultItem> calculateResult(int competitionId);
}
