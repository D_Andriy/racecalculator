package com.fau.driver.service;

import com.fau.driver.domain.Driver;
import com.fau.driver.domain.FrequentDriverSeason;

import java.util.List;

public interface DriverService {

    void createDriver(int competitionId, Driver driver);

    List<Driver> getDriverList(int competitionId);

    List<Driver> getDriversBySurname(String surname);

    void deleteDriver(int driverId);

    List<Driver> getOrderedDriversByQualificationResult(int competitionId);

    List<Driver> getGeneralDriversAndLaps(int competitionId);

    List<FrequentDriverSeason> getTopFrequentDriversInSeason();

    void updateDriver(Driver driver);

    Driver getDriverById(int driverId);
}
