package com.fau.driver.service;

import com.fau.driver.domain.Driver;
import com.fau.driver.domain.FrequentDriverSeason;
import com.fau.driver.repository.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DriverServiceImpl implements com.fau.driver.service.DriverService {

    private final DriverRepository driverRepository;

    @Autowired
    public DriverServiceImpl(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    @Override
    public void createDriver(int competitionId, Driver driver) {
        driverRepository.createDriver(competitionId, driver);
    }

    @Override
    public List<Driver> getDriverList(int competitionId) {
        return driverRepository.getDriverList(competitionId);
    }

    @Override
    public List<Driver> getDriversBySurname(String surname) {
        return driverRepository.getDriversBySurname(surname);
    }

    @Override
    public void deleteDriver(int driverId) {
        driverRepository.deleteDriver(driverId);
    }

    @Override
    public List<Driver> getOrderedDriversByQualificationResult(int competitionId) {
        return driverRepository.getOrderedDriversByQualificationResult(competitionId);
    }

    @Override
    public List<Driver> getGeneralDriversAndLaps(int competitionId){
        return driverRepository.getGeneralDriversAndLaps(competitionId);
    }

    @Override
    public List<FrequentDriverSeason> getTopFrequentDriversInSeason() {
        return driverRepository.getTopFrequentDriversInSeason();
    }

    @Override
    public void updateDriver(Driver driver) {
        driverRepository.updateDriver(driver);
    }

    @Override
    public Driver getDriverById(int driverId) {
        return driverRepository.getDriverById(driverId);
    }
}

