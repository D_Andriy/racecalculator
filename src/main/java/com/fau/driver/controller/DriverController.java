package com.fau.driver.controller;

import com.fau.driver.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/driver")
public class DriverController {

    private final DriverService driverService;

    @Autowired
    public DriverController(DriverService driverService) {
        this.driverService = driverService;
    }

    @GetMapping("/search/surname")
    public String getDriverBySurname(@RequestParam String driverSurname, Model model) {
        model.addAttribute("drivers", driverService.getDriversBySurname(driverSurname));
        return "searchBySurname";
    }

    @GetMapping("/rating")
    public String getTopFrequentDriversInSeason(Model model){
        model.addAttribute("drivers", driverService.getTopFrequentDriversInSeason());
        return "topFrequentDrivers";
    }
}
