package com.fau.driver.repository;

import com.fau.driver.domain.FrequentDriverSeason;
import com.fau.driver.repository.rowmapper.DriverRowMapper;
import com.fau.driver.domain.Driver;
import com.fau.driver.repository.rowmapper.DriversExtractor;
import com.fau.driver.repository.rowmapper.FrequentDriverSeasonRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DriverRepositoryImpl implements DriverRepository {

    private static final int TOP_COUNT_RATING = 10;
    private static final int LAP_QUALIFICATION_NUMBER = 0;
    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert insertDriver;

    @Autowired
    public DriverRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.insertDriver = new SimpleJdbcInsert(dataSource)
                .usingGeneratedKeyColumns("id")
                .withTableName("driver");
    }

    @Override
    public void createDriver(int competitionId, Driver driver) {
        Map<String, Object> parameters = new HashMap<>(6);
        parameters.put("number", driver.getNumber());
        parameters.put("name", driver.getName());
        parameters.put("surname", driver.getSurname());
        parameters.put("car_category", driver.getCarCategory());
        parameters.put("car_mark", driver.getCarMark());
        Number newId = insertDriver.executeAndReturnKey(parameters);
        driver.setId(newId.intValue());

        addDriverToCompetition(competitionId, newId.intValue());
    }

    @Override
    public void addDriverToCompetition(int competitionId, int driverId) {
        Object[] params = new Object[]{competitionId, driverId};
        String addDriverToCompetition = "INSERT INTO competition_driver (competition_id, driver_id) VALUES(?, ?)";
        jdbcTemplate.update(addDriverToCompetition, params);
    }

    @Override
    public List<Driver> getDriverList(int competitionId) {
        Object[] param = new Object[]{competitionId};
        String driverList = "SELECT " +
                            "d.id," +
                            "d.name," +
                            "d.surname," +
                            "d.number," +
                            "d.car_category," +
                            "d.car_mark\n" +
                            "FROM driver d\n" +
                            "JOIN competition_driver cd ON d.id = cd.driver_id\n" +
                            "AND cd.competition_id = ?";

        return jdbcTemplate.query(driverList, param, new DriverRowMapper());
    }

    @Override
    @Transactional
    public void deleteDriver(int driverId) {
        Object[] param = new Object[]{driverId};
        String deleteFromCompetitionDriver = "DELETE FROM competition_driver " +
                                             "WHERE driver_id=?";
        String deleteFromDriver = "DELETE FROM driver " +
                                  "WHERE id=?";
        jdbcTemplate.update(deleteFromCompetitionDriver, param);
        jdbcTemplate.update(deleteFromDriver, param);
    }

    @Override
    public void updateDriver(Driver driver) {
        Object[] params = new Object[]{driver.getName(), driver.getSurname(), driver.getNumber(),
                driver.getCarCategory(), driver.getCarMark(), driver.getId()};
        String updateDriver = "UPDATE driver " +
                              "SET name = ?, surname = ?, number = ?, car_category = ?, car_mark = ? WHERE id = ?";
        jdbcTemplate.update(updateDriver, params);
    }

    @Override
    public List<Driver> getDriversBySurname(String surname) {
        Object[] param = new Object[]{surname};
        String getDriversBySurname = "SELECT " +
                                     "d.id," +
                                     "d.name," +
                                     "d.surname," +
                                     "d.number," +
                                     "d.car_category," +
                                     "d.car_mark\n" +
                                     "FROM driver d WHERE surname = ?";

        return jdbcTemplate.query(getDriversBySurname, param, new DriverRowMapper());
    }

    @Override
    public List<Driver> getOrderedDriversByQualificationResult(int competitionId) {
        Object[] param = new Object[]{competitionId};
        String orderDriversByResult = "SELECT " +
                                      "d.id," +
                                      "d.name," +
                                      "d.surname," +
                                      "d.number," +
                                      "d.car_category," +
                                      "d.car_mark," +
                                      "lp.driver_id," +
                                      "lp.lap_number," +
                                      "lp.chip_board," +
                                      "lp.chip_front," +
                                      "lp.false_start," +
                                      "lp.time\n" +
                                      "FROM driver d\n" +
                                      "JOIN competition_driver cd ON d.id = cd.driver_id\n" +
                                      "JOIN lap lp ON lp.driver_id = d.id " +
                                      "AND cd.competition_id=? " +
                                      "AND lp.lap_number = " + LAP_QUALIFICATION_NUMBER + " " +
                                      "ORDER BY lp.time";

        return jdbcTemplate.query(orderDriversByResult, param, new DriversExtractor());
    }

    @Override
    public List<Driver> getGeneralDriversAndLaps(int competitionId){
        Object[] param = new Object[]{competitionId};
        String generalDriversAndLaps = "SELECT " +
                                       "d.id," +
                                       "d.name," +
                                       "d.surname," +
                                       "d.number," +
                                       "d.car_category," +
                                       "d.car_mark," +
                                       "lp.driver_id," +
                                       "lp.lap_number," +
                                       "lp.chip_board," +
                                       "lp.chip_front," +
                                       "lp.false_start," +
                                       "lp.time\n" +
                                       "FROM driver d\n" +
                                       "JOIN competition_driver cd ON d.id = cd.driver_id\n" +
                                       "JOIN lap lp ON lp.driver_id = d.id " +
                                       "AND cd.competition_id=? " +
                                       "AND lp.lap_number > " + LAP_QUALIFICATION_NUMBER;

        return jdbcTemplate.query(generalDriversAndLaps, param, new DriversExtractor());
    }

    @Override
    public Driver getDriverById(int driverId) {
        Object[] param = new Object[]{driverId};
        String getDriverById = "SELECT " +
                               "d.id," +
                               "d.name," +
                               "d.surname," +
                               "d.number," +
                               "d.car_category," +
                               "d.car_mark\n" +
                               "FROM driver d WHERE id = ?";

        return jdbcTemplate.queryForObject(getDriverById, param, new DriverRowMapper());
    }

    @Override
    public List<FrequentDriverSeason> getTopFrequentDriversInSeason() {
        String getTopDrivers = "SELECT " +
                               "DISTINCT " +
                               "d.surname, " +
                               "d.name, " +
                               "COUNT(d.surname) AS count " +
                               "FROM driver d\n" +
                               "JOIN competition_driver cd ON d.id = cd.driver_id\n" +
                               "JOIN competition c ON cd.competition_id = c.id\n" +
                               "WHERE YEAR(c.date) = YEAR(CURDATE())\n" +
                               "GROUP BY d.surname\n" +
                               "ORDER BY count DESC\n" +
                               "LIMIT " + TOP_COUNT_RATING;
        return jdbcTemplate.query(getTopDrivers, new FrequentDriverSeasonRowMapper());
    }
}
