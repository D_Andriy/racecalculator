package com.fau.driver.repository;

import com.fau.driver.domain.Driver;
import com.fau.driver.domain.FrequentDriverSeason;

import java.util.List;

public interface DriverRepository {

    void createDriver(int competitionId, Driver driver);

    void addDriverToCompetition(int competitionId, int driverId);

    List<Driver> getDriverList(int competitionId);

    void deleteDriver(int driverId);

    void updateDriver(Driver driver);

    List<Driver> getDriversBySurname(String surname);

    List<Driver> getOrderedDriversByQualificationResult(int competitionId);

    List<Driver> getGeneralDriversAndLaps(int competitionId);

    Driver getDriverById(int driverId);

    List<FrequentDriverSeason> getTopFrequentDriversInSeason();
}
