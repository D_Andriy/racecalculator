package com.fau.driver.repository.rowmapper;

import com.fau.driver.domain.FrequentDriverSeason;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FrequentDriverSeasonRowMapper implements RowMapper<FrequentDriverSeason> {

    @Override
    public FrequentDriverSeason mapRow(ResultSet rs, int rowNum) throws SQLException {
        FrequentDriverSeason frequentDriverSeason = new FrequentDriverSeason();
        frequentDriverSeason.setDriverSurname(rs.getString("surname"));
        frequentDriverSeason.setDriverName(rs.getString("name"));
        frequentDriverSeason.setQuantityCompetition(rs.getInt("count"));

        return frequentDriverSeason;
    }
}
