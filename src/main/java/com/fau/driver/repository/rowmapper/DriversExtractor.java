package com.fau.driver.repository.rowmapper;

import com.fau.driver.domain.Driver;
import com.fau.lap.domain.Lap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DriversExtractor implements ResultSetExtractor<List<Driver>> {

    @Override
    public List<Driver> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Integer, Driver> checkingDrivers = new HashMap<>();
        List<Driver> drivers = new ArrayList<>();

        while (rs.next()) {
            int id = rs.getInt("id");
            Driver driver = checkingDrivers.get(id);
            if (driver == null) {
                driver = new Driver();
                driver.setId(rs.getInt("id"));
                driver.setName(rs.getString("name"));
                driver.setSurname(rs.getString("surname"));
                driver.setNumber(rs.getInt("number"));
                driver.setCarCategory(rs.getString("car_category"));
                driver.setCarMark(rs.getString("car_mark"));
                checkingDrivers.put(id, driver);
                drivers.add(driver);
            }

            int driverId = rs.getInt("driver_id");
            if (driverId == driver.getId()) {
                List<Lap> laps = driver.getLaps();
                if (laps == null) {
                    laps = new ArrayList<>();
                    driver.setLaps(laps);
                }

                Lap lap = new Lap();
                lap.setDriver_id(driverId);
                lap.setLapNumber(rs.getInt("lap_number"));
                lap.setChipBoard(rs.getBoolean("chip_board"));
                lap.setChipFront(rs.getBoolean("chip_front"));
                lap.setFalseStart(rs.getBoolean("false_start"));
                lap.setTime(LocalTime.ofNanoOfDay(rs.getLong("time")));

                laps.add(lap);
            }
        }

        return drivers;
    }
}
