package com.fau.driver.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum CarCategoryType {

    STANDARD("СТАНДАРТ"),
    SPORT_FRONT("СПОРТ ПЕРЕДНІЙ"),
    SPORT_REAR("СПОРТ ЗАДНІЙ");

    @Getter
    private String categoryName;
}
