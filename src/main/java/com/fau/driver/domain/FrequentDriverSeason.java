package com.fau.driver.domain;

import lombok.Data;

@Data
public class FrequentDriverSeason {

    private String driverSurname;
    private String driverName;
    private int quantityCompetition;
}
