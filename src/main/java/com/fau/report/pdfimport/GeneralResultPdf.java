package com.fau.report.pdfimport;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.Map;

public abstract class GeneralResultPdf extends AbstractView {

    public GeneralResultPdf() {
        setContentType("application/pdf");
    }

    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model,
                                           HttpServletRequest request, HttpServletResponse response) throws Exception {

        ByteArrayOutputStream byteArrayOutputStream = createTemporaryOutputStream();

        Document document = new Document(PageSize.A4);
        PdfWriter writer = PdfWriter.getInstance(document, byteArrayOutputStream);

        document.open();
        buildPdfDocument(model, document, writer);
        document.close();
        writeToResponse(response, byteArrayOutputStream);
    }

    protected abstract void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer) throws Exception;
}
