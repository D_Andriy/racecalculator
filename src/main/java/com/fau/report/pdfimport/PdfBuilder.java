package com.fau.report.pdfimport;

import com.fau.competition.domain.DriverResultItem;
import com.fau.driver.domain.CarCategoryType;
import com.fau.util.PercentileCalculator;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class PdfBuilder extends GeneralResultPdf {

    private static final String FONT = "/fonts/arial.ttf";

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document,
                                    PdfWriter writer) throws Exception {
        List<DriverResultItem> generalDriversResult = (List<DriverResultItem>) model.get("generalDriversResult");

        Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, true);
        font.setColor(BaseColor.BLACK);

        document.add(new Paragraph("Результати змагання.", font));
        document.add(new Paragraph("Загальна таблиця результатів:", font));
        absoluteTable(generalDriversResult, document, font);
        document.add(new Paragraph("таблиця результатів \"СТАНДАРТ\":", font));
        standardCategoryTable(generalDriversResult, document, font);
        document.add(new Paragraph("таблиця результатів \"СПОРТ ПЕРЕДНІЙ\":", font));
        frontCategoryTable(generalDriversResult, document, font);
        document.add(new Paragraph("таблиця результатів \"СПОРТ ЗАДНІЙ\":", font));
        rearCategoryTable(generalDriversResult, document, font);
        document.add(new Paragraph("статистика результатів часу:", font));
        percentileTable(generalDriversResult, document, font);
    }

    private void rearCategoryTable(List<DriverResultItem> generalDriversResult,
                                   Document document, Font font) throws DocumentException {
        PdfPTable rearCategoryTable = new PdfPTable(6);
        rearCategoryTable.setWidthPercentage(100.0f);
        rearCategoryTable.setWidths(new int[]{5, 35, 15, 15, 15, 15});
        rearCategoryTable.setSpacingBefore(10);

        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.YELLOW);
        cell.setPadding(5);

        headTable(rearCategoryTable, cell, font);

        AtomicInteger count = new AtomicInteger();
        generalDriversResult.stream()
                .filter(driver -> driver.getDriverCarCategory().equals(CarCategoryType.STANDARD.getCategoryName()))
                .forEach(driver -> {
                    rearCategoryTable.addCell(String.valueOf(count.incrementAndGet()));
                    tableContent(driver, rearCategoryTable, font);
                });

        document.add(rearCategoryTable);
    }

    private void frontCategoryTable(List<DriverResultItem> generalDriversResult,
                                    Document document, Font font) throws DocumentException {
        PdfPTable frontCategoryTable = new PdfPTable(6);
        frontCategoryTable.setWidthPercentage(100.0f);
        frontCategoryTable.setWidths(new int[]{5, 35, 15, 15, 15, 15});
        frontCategoryTable.setSpacingBefore(10);

        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.RED);
        cell.setPadding(5);

        headTable(frontCategoryTable, cell, font);

        AtomicInteger count = new AtomicInteger();
        generalDriversResult.stream()
                .filter(driver -> driver.getDriverCarCategory().equals(CarCategoryType.SPORT_FRONT.getCategoryName()))
                .forEach(driver -> {
                    frontCategoryTable.addCell(String.valueOf(count.incrementAndGet()));
                    tableContent(driver, frontCategoryTable, font);
                });

        document.add(frontCategoryTable);
    }

    private void standardCategoryTable(List<DriverResultItem> generalDriversResult,
                                       Document document, Font font) throws DocumentException {
        PdfPTable standardCategoryTable = new PdfPTable(6);
        standardCategoryTable.setWidthPercentage(100.0f);
        standardCategoryTable.setWidths(new int[]{5, 35, 15, 15, 15, 15});
        standardCategoryTable.setSpacingBefore(10);

        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.GREEN);
        cell.setPadding(5);

        headTable(standardCategoryTable, cell, font);

        AtomicInteger count = new AtomicInteger();
        generalDriversResult.stream()
                .filter(driver -> driver.getDriverCarCategory().equals(CarCategoryType.SPORT_REAR.getCategoryName()))
                .forEach(driver -> {
                    standardCategoryTable.addCell(String.valueOf(count.incrementAndGet()));
                    tableContent(driver, standardCategoryTable, font);
                });

        document.add(standardCategoryTable);
    }

    private void absoluteTable(List<DriverResultItem> generalDriversResult,
                               Document document, Font font) throws DocumentException {
        PdfPTable absoluteTable = new PdfPTable(6);
        absoluteTable.setWidthPercentage(100.0f);
        absoluteTable.setWidths(new int[]{5, 35, 15, 15, 15, 15});
        absoluteTable.setSpacingBefore(10);

        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setPadding(5);

        headTable(absoluteTable, cell, font);

        AtomicInteger count = new AtomicInteger();
        generalDriversResult.stream()
                .forEach(driver -> {
                    absoluteTable.addCell(String.valueOf(count.incrementAndGet()));
                    tableContent(driver, absoluteTable, font);
                });

        document.add(absoluteTable);
    }

    private void tableContent(DriverResultItem driver, PdfPTable table, Font font) {
        PdfPCell cell2 = new PdfPCell();
        cell2.setPhrase(new Phrase(driver.getDriverName(), font));
        table.addCell(cell2);
        table.addCell(String.valueOf(driver.getFirstLap()));
        table.addCell(String.valueOf(driver.getSecondLap()));
        table.addCell(String.valueOf(driver.getThirdLap()));
        table.addCell(String.valueOf(driver.getAverage()));
    }

    private void headTable(PdfPTable table, PdfPCell cell, Font font) {
        cell.setPhrase(new Phrase("Місце", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Прізвище та Ім'я", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Результат 1-го заїзду", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Результат 2-го заїзду", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Результат 3-го заїзду", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Загальний результат", font));
        table.addCell(cell);
    }

    private void percentileTable(List<DriverResultItem> generalDriversResult,
                                 Document document, Font font) throws DocumentException {
        PdfPTable percentileTable = new PdfPTable(3);
        percentileTable.setWidthPercentage(100.0f);
        percentileTable.setSpacingBefore(10);
        PdfPCell cell = new PdfPCell();

        cell.setPhrase(new Phrase("1-й квартиль", font));
        percentileTable.addCell(cell);
        cell.setPhrase(new Phrase("медіана", font));
        percentileTable.addCell(cell);
        cell.setPhrase(new Phrase("3-й квартиль", font));
        percentileTable.addCell(cell);

        percentileTable.addCell(String.valueOf(PercentileCalculator.percentile(generalDriversResult.stream()
                .map(DriverResultItem::getAverage)
                .collect(Collectors.toList()), 25)));

        percentileTable.addCell(String.valueOf(PercentileCalculator.percentile(generalDriversResult.stream()
                .map(DriverResultItem::getAverage)
                .collect(Collectors.toList()), 50)));

        percentileTable.addCell(String.valueOf(PercentileCalculator.percentile(generalDriversResult.stream()
                .map(DriverResultItem::getAverage)
                .collect(Collectors.toList()), 75)));

        document.add(percentileTable);
    }
}
