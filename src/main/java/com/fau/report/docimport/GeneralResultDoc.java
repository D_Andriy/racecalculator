package com.fau.report.docimport;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.Map;

public abstract class GeneralResultDoc extends AbstractView {

    public GeneralResultDoc() {
        setContentType("application/msword");
    }

    @Override
    protected boolean generatesDownloadContent() {
        return true;
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
                                           HttpServletResponse response) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = createTemporaryOutputStream();

        XWPFDocument document = new XWPFDocument();
        buildDocDocument(model, document);
        byteArrayOutputStream.close();
        document.write(byteArrayOutputStream);
        writeToResponse(response, byteArrayOutputStream);
    }

    protected abstract void buildDocDocument(Map<String, Object> model, XWPFDocument document) throws Exception;
}
