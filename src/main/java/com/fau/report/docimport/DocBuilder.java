package com.fau.report.docimport;

import com.fau.competition.domain.DriverResultItem;
import com.fau.driver.domain.CarCategoryType;
import com.fau.util.PercentileCalculator;
import org.apache.poi.xwpf.usermodel.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class DocBuilder extends GeneralResultDoc {

    @Override
    protected void buildDocDocument(Map<String, Object> model, XWPFDocument document) throws Exception {
        List<DriverResultItem> generalDriversResult = (List<DriverResultItem>) model.get("generalDriversResult");

        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);

        XWPFRun run = paragraph.createRun();
        run.setText("Результати змагання.");

        absoluteTable(generalDriversResult, document);
        standardCategoryTable(generalDriversResult, document);
        frontCategoryTable(generalDriversResult, document);
        rearCategoryTable(generalDriversResult, document);
        percentileTable(generalDriversResult, document);
    }

    private void rearCategoryTable(List<DriverResultItem> generalDriversResult, XWPFDocument document) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("Таблиця результатів \"СПОРТ ЗАДНІЙ\"");

        XWPFTable rearCategoryTable = document.createTable();
        headTable(rearCategoryTable);

        AtomicInteger count = new AtomicInteger();
        generalDriversResult.stream()
                .filter(driver -> driver.getDriverCarCategory().equals(CarCategoryType.SPORT_REAR.getCategoryName()))
                .forEach(driver -> {
                    XWPFTableRow tableRowContent = rearCategoryTable.createRow();
                    tableRowContent.getCell(0).setText(String.valueOf(count.incrementAndGet()));
                    tableContent(driver, tableRowContent);
                });
    }

    private void frontCategoryTable(List<DriverResultItem> generalDriversResult, XWPFDocument document) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("Таблиця результатів \"СПОРТ ПЕРЕДНІЙ\"");

        XWPFTable frontCategoryTable = document.createTable();
        headTable(frontCategoryTable);

        AtomicInteger count = new AtomicInteger();
        generalDriversResult.stream()
                .filter(driver -> driver.getDriverCarCategory().equals(CarCategoryType.SPORT_FRONT.getCategoryName()))
                .forEach(driver -> {
                    XWPFTableRow tableRowContent = frontCategoryTable.createRow();
                    tableRowContent.getCell(0).setText(String.valueOf(count.incrementAndGet()));
                    tableContent(driver, tableRowContent);
                });
    }

    private void standardCategoryTable(List<DriverResultItem> generalDriversResult, XWPFDocument document) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("Таблиця результатів \"СТАНДАРТ\"");

        XWPFTable standardCategoryTable = document.createTable();
        headTable(standardCategoryTable);

        AtomicInteger count = new AtomicInteger();
        generalDriversResult.stream()
                .filter(driver -> driver.getDriverCarCategory().equals(CarCategoryType.STANDARD.getCategoryName()))
                .forEach(driver -> {
                    XWPFTableRow tableRowContent = standardCategoryTable.createRow();
                    tableRowContent.getCell(0).setText(String.valueOf(count.incrementAndGet()));
                    tableContent(driver, tableRowContent);
                });
    }

    private void absoluteTable(List<DriverResultItem> generalDriversResult, XWPFDocument document) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("Загальна таблиця результатів");

        XWPFTable absoluteTable = document.createTable();
        headTable(absoluteTable);

        AtomicInteger count = new AtomicInteger();
        generalDriversResult.stream()
                .forEach(driver -> {
                    XWPFTableRow tableRowContent = absoluteTable.createRow();
                    tableRowContent.getCell(0).setText(String.valueOf(count.incrementAndGet()));
                    tableContent(driver, tableRowContent);
                });
    }

    private void tableContent(DriverResultItem driver, XWPFTableRow tableRowContent) {
        tableRowContent.getCell(1).setText(driver.getDriverName());
        tableRowContent.getCell(2).setText(String.valueOf(driver.getFirstLap()));
        tableRowContent.getCell(3).setText(String.valueOf(driver.getSecondLap()));
        tableRowContent.getCell(4).setText(String.valueOf(driver.getThirdLap()));
        tableRowContent.getCell(5).setText(String.valueOf(driver.getAverage()));
    }

    private void headTable(XWPFTable table) {
        XWPFTableRow tableRow = table.getRow(0);
        tableRow.getCell(0).setText("Місце");
        tableRow.addNewTableCell().setText("Прізвище Ім'я");
        tableRow.addNewTableCell().setText("результати 1-го заїзду");
        tableRow.addNewTableCell().setText("результати 2-го заїзду");
        tableRow.addNewTableCell().setText("результати 3-го заїзду");
        tableRow.addNewTableCell().setText("загальний результат");
    }

    private void percentileTable(List<DriverResultItem> generalDriversResult, XWPFDocument document) {
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();
        run.setText("Статистика загального часу:");

        XWPFTable percentileTable = document.createTable();
        XWPFTableRow tableRow = percentileTable.getRow(0);
        tableRow.getCell(0).setText("1-й квартиль");
        tableRow.addNewTableCell().setText("медиана");
        tableRow.addNewTableCell().setText("2-й квартиль");

        XWPFTableRow tableRowContent = percentileTable.createRow();
        tableRowContent.getCell(0).setText(String.valueOf(PercentileCalculator.percentile(generalDriversResult.stream()
                .map(DriverResultItem::getAverage)
                .collect(Collectors.toList()), 25)));

        tableRowContent.getCell(1).setText(String.valueOf(PercentileCalculator.percentile(generalDriversResult.stream()
                .map(DriverResultItem::getAverage)
                .collect(Collectors.toList()), 50)));

        tableRowContent.getCell(2).setText(String.valueOf(PercentileCalculator.percentile(generalDriversResult.stream()
                .map(DriverResultItem::getAverage)
                .collect(Collectors.toList()), 75)));
    }
}
