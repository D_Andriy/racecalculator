package com.fau.report.сontroller;

import com.fau.competition.service.CompetitionService;
import com.fau.report.docimport.DocBuilder;
import com.fau.report.pdfimport.PdfBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ImportController {

    private final CompetitionService competitionService;

    @Autowired
    public ImportController(CompetitionService competitionService) {
        this.competitionService = competitionService;
    }

    @GetMapping("/{competitionId}/downloadPdf")
    public PdfBuilder downloadPdf(@PathVariable int competitionId, Model model) {
        model.addAttribute("generalDriversResult", competitionService.calculateResult(competitionId));
        return new PdfBuilder();
    }

    @PostMapping("{competitionId}/downloadDoc")
    public DocBuilder downloadDoc(@PathVariable int competitionId, Model model) {
        model.addAttribute("generalDriversResult", competitionService.calculateResult(competitionId));
        return new DocBuilder();
    }
}
